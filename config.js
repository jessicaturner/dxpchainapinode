/**
 * config.js for dxpchainAPINode
 * (c) 2017 TheSerranos.
 * This code is released under the 
 * terms of the MIT license. 
 */
var config = {};
// Secure WebSocket settings
// < - - >
// Change it to:
// Germany; 'wss://dxpchain.openledger.info/ws'
// Hong Kong; 'wss://bit.dxpabc.org/ws'
// Hong Kong; 'wss://openledger.hk/ws'
// Hangzhou, China; 'wss://dxp.transwiser.com/ws'
// Hangzhou, China; 'wss://dxpchain.dacplay.org:8089/ws'
// Toronto, Canada; 'wss://secure.freedomledger.com/ws'
// < - - > 
// Public testnet; 'wss://testnet.dxpchain.eu/ws'
config.ws = {
    host: 'wss://127.0.0.1:11011',
};
// api listen on port;
config.api = {
    port: 3333,
    code: {
    	name: 'dxpchainAPINode',
    	version: '0.0.3b'
    }
};
module.exports = config;